# Execl_WordTOPDF

#### 介绍
C#  Execl,Word 转成PDF文件，支持性非常好，速度转换快 Windows下,无需安装Office

#### 直接下载C#发行版
https://gitee.com/bandung/Execl_WordTOPDF/releases/V1.0

#### 安装教程

1. 纯命令的,用的Aspose进行封装

#### execl转pdf

2. .\WordToPdf.exe -execl C:\Users\Administrator\Desktop\1.xls  C:\Users\Administrator\Desktop\test.pdf

#### word转pdf
3. .\WordToPdf.exe -word C:\Users\Administrator\Desktop\1.doc  C:\Users\Administrator\Desktop\test.pdf

#### ppt转pdf
3. .\WordToPdf.exe -ppt C:\Users\Administrator\Desktop\1.ppt  C:\Users\Administrator\Desktop\test.pdf

.NetCore 平台下不兼容
